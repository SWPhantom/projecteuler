import java.util.*;
import java.math.*;

public class prob413{
public static void main(String[] args){
	////Description:
	/*
		http://projecteuler.net/problem=413
	*/
	System.out.println(	"\n----------------------------------------------"+
								"\nThis program takes a command line input 'n'."+
								"\nIt then finds the number of 'one-child num-'"+
								"\nbers below 'n'."+
								"\nInspect source code for more information."+
								"\n----------------------------------------------\n");
	////------------------------------

	////Declarations:
	String temp = args[0];
	BigInteger input = new BigInteger(temp);
	long childSum = 0;
	OneChildHelper childCheck = new OneChildHelper();
	////------------------------------
	
	////Hard work block
	BigInteger nothing = new BigInteger("0");
	BigInteger single = new BigInteger("1");
	for(BigInteger i = input; i.compareTo(nothing) == 1 ; i = i.subtract(single)){
		//System.out.println("Number "+i.toString()+" is being analyzed.");
		BigInteger tenThou = new BigInteger("100000");
		if(i.mod(tenThou).compareTo(nothing) == 0){//Debug code that lets me see if program is doing something.
			System.out.println("Number "+i.toString()+" is being analyzed.");
		}
		if(childCheck.hasOneChild(i)){
			++childSum;
		}
	}
	System.out.println("Total one-child numbers under (and including) "+input+": "+childSum);
		/*
		if(i%100==0){ 
		}
		*/
	System.exit(0);
	////------------------------------
}
}
