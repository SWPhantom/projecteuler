var amicableArray = new Array();

var sumDivisors = function(input){
    var sum = 1;
    var square = Math.sqrt(input);
    for(var i = 2; i < square; ++i) {
        if(input % i == 0){
            sum += i + (input/i);
        }
    }
    
    if(square % 1 == 0){
        sum += square;
    }
    
    return sum;
};

var validateAmicability = function(input){
    if(amicableArray[input] == 0) return false;
    if(amicableArray[input] == input || amicableArray[amicableArray[input]] != input){
        amicableArray[input] = 0;
        return false;
    }
    
    return true;
}

var main = function(maximum){
    var sum = 0;
    // Reset Array of amicable numbers.
    for(var i = 0; i < maximum; ++i){
        amicableArray[i] = -1;
    }
    
    amicableArray[0] = 0;
    amicableArray[1] = 0;
    
    for(var i = 2; i < maximum; ++i){
        var currentDivSum = sumDivisors(i);
        // If this number references anything smaller that has been shown to point
        // at an invalid number, it is also invalid.
        if(currentDivSum > maximum || amicableArray[currentDivSum] == 0){
            amicableArray[i] = 0;
        } else {
            amicableArray[i] = currentDivSum;
        }
    }
    
    // Find Amicable pairs
    for(var i = 2; i < maximum; ++i){
        if(validateAmicability(i)){
            sum += i;
        }
    }
    
    for(var i = 2; i < maximum; ++i){
        console.log("DivisorSum(" + i + ") = " + amicableArray[i]);
    }
    
    console.log(sum);
}

main(10001);