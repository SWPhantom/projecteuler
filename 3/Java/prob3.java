import java.util.*;
import java.math.*;

public class prob3{
public static void main(String[] args){
	//declarations
	int[] primes;
	long input = 0;
	PrimeCheck checker= new PrimeCheck();
	//------------------------------

	//sanity check single argument input block.
	if (args.length > 0){
			try{
				input = new Long(args[0]);
			}catch (NumberFormatException e) {
				System.err.println("Argument must be an integer or Long. Exiting.");
				System.exit(1);
			}
	}
	//------------------------------
	
	//Hard work block
	int temp = 0;
	temp = (int)(Math.sqrt(input));
	//temp = (int)(input/2);
	System.out.println(temp);
	System.out.println("Checking: ");
	for(int i = temp; i > 2; --i){
		System.out.print(i+",");
		if(i%10 == 0){System.out.print("\n");}
		if(checker.isPrime(i)){
			if(input%i == 0){
				System.out.println("\n\nLargest Prime: "+i);
				System.exit(0);
			}
		}
	}
	
	
	
}
}
