import java.util.*;
import java.math.*;

public class prob14{
public static void main(String[] args){
	////Description:
	System.out.println("\nThis program takes a command line input 'n'.");
	System.out.println("It then finds the first triangle number that");
	System.out.println("has 'n' divisors.");
	System.out.println("Inspect source code for more information.");
	////------------------------------

	////Declarations:
	int input = 0;
	int maxSequence = 0;
	int index = 0;
	int temp = 0;
	ColHelper colCheck = new ColHelper();
	////------------------------------

	////sanity check single argument input block.
	if (args.length == 1){
			try{
				input = Integer.parseInt(args[0]);
			}catch (NumberFormatException e) {
				System.err.println("Argument must be an integer. Exiting.");
				System.exit(1);
			}
	}else{
		System.out.println("Needs 1 argument!");
		System.exit(1);
	}
	////------------------------------
	
	////Hard work block
	for(int i = 1; i < input; ++i){
		temp = colCheck.checkCollatzLength(i);
		
		/*
		if(i%100==0){ //Debug code that lets me see if program is doing something.
			System.out.println("Number "+i+" has Collatz sequence length of "+temp+".");
		}
		*/
		
		if(temp > maxSequence){	//Update the maximums.
			maxSequence = temp;
			index = i;
		}
	}
	System.out.println("Number "+index+" has maximum Collatz sequence length of "+maxSequence+".");
	System.exit(0);
	////------------------------------
}
}
