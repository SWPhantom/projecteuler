import java.util.*;
/**
 * This class is a helper class for checking to see how
 * many divisors an input number has.
 */

public class ColHelper{
	
	/**
	 * Default constructor. I suppose I don't need to 
	 * instantiate the object...
	 */
	public ColHelper(){}
	
	/**	
	 * @param input	-The integer passed from main routine.
	 * @return 			-Returns length of the Collatz Sequence.
	 */
	public int checkCollatzLength(int input){
		long data = (long)input;	//Using long due to in size constraints.
		int sequenceLength = 1;	//Starting at 1 because base case in absorbed by the while loop.
		while(data > 1){
			if(data%2 == 0){	//Even case for Collatz sequence
				data /= 2;
			}else{	//odd case for Collatz Sequence
				data = data*3+1;
			}
			++sequenceLength;
		}
		return sequenceLength;
	}

}
