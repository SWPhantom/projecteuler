import java.util.*;
import java.io.*;
import java.math.*;

public class prob15{
public static void main(String[] args){
	////Description:
	/*
		http://projecteuler.net/problem=15
	*/
	System.out.println(	"\n----------------------------------------------"+
								"\nThis program takes in an int N, creates an NxN"+
								"\narray with 1s along the upper and left cells,"+
								"\nand calculates the rest of the cells by adding"+
								"\nneighboring cells (Pascal's Triangle)."+
								"\nInspect source code for more information."+
								"\n----------------------------------------------\n");
	////-------------------------------

	////Declarations:
	String input = "";
	int size = 0;
	long matrix[][];
	////------------------------------

	////Sanity checks.
	//Check number of inputs.
	if (args.length == 1){
		input = args[0];
	}else{
		System.out.println("Needs 1 argument!");
		System.exit(1);
	}
	
	//Check for input format error.
	try{
		size = Integer.parseInt(input);
	}catch(NumberFormatException e){
		System.out.println("Need to input a number.\n");
		System.exit(1);
	}
	////------------------------------
	
	////Hard work block
	matrix = new long[size][size];
	//Populate the first row and column 
	for(int i = 0; i < size; ++i){
		matrix[0][i] = 1;
		matrix[i][0] = 1;
	}
	
	//Start calculating cells!
	for(int i = 1; i < size; ++i){
		for(int j = 1; j < size; ++j){
			matrix[i][j] = matrix[i-1][j]+matrix[i][j-1];
		}
	}
	//Debug
	/*
	for(int i = 0; i < size; ++i){
		for(int j = 0; j < size; ++j){
			System.out.print(matrix[i][j]+".");
		}
		System.out.println();
	}
	*/
	
	System.out.println("Answer: "+matrix[size-1][size-1]);
	System.exit(0);
	////------------------------------
}
}
