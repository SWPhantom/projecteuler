import java.util.*;
import java.io.*;
import java.math.*;

public class prob18{
public static void main(String[] args){
	////Description:
	/*
		http://projecteuler.net/problem=18
	*/
	System.out.println(	"\n----------------------------------------------"+
								"\nThis program reads an input text specified via"+
								"\ncommand line. It reads the numbers within and"+
								"\nconstructs an ArrayList for"+
								"\nevery row of numbers. It then finds the path"+
								"\nthat yields the greatest sum."+
								"\nInspect source code for more information."+
								"\n----------------------------------------------\n");
	////-------------------------------

	////Declarations:
	String input = "";
	ArrayList<ArrayList<Integer>> matrix = new ArrayList<ArrayList<Integer>>();
	ArrayList<String> rawStrings = new ArrayList<String>();
	long maxSequence = 0;
	////------------------------------

	////sanity check single argument input block.
	if (args.length == 1){
		input = args[0];
	}else{
		System.out.println("Needs 1 argument!");
		System.exit(1);
	}
	////------------------------------
	
	////Hard work block
	
	try {//Block to take in the contents of the file
		BufferedReader in = new BufferedReader(new FileReader(input));
		String str;
		while ((str = in.readLine()) != null)
			rawStrings.add(str);
			in.close();
	}catch(IOException e){}
	
	//Put the input strings into the int array.
	for(int i = 0; i < rawStrings.size(); ++i){
		String [] fields = rawStrings.get(i).split(" ");
		ArrayList<Integer> row = new ArrayList<Integer>();
		row.clear();
		for(int j = 0; j < fields.length; ++j){
			row.add(Integer.parseInt(fields[j]));
		}
		matrix.add(row);
	}
	
	/*//Debug
	for(int i = 0; i < matrix.size(); ++i){
		for(int j = 0; j <matrix.get(i).size(); ++j){
			System.out.print(matrix.get(i).get(j)+".");
		}
		System.out.println();
	}
	*/
	
	for(int i = matrix.size()-1; i > 0; --i){
		for(int j = 0; j < matrix.get(i).size()-1; ++j){
			int tempa = matrix.get(i).get(j);
			int tempb = matrix.get(i).get(j+1);
			int tempc = matrix.get(i-1).get(j);
			if(tempa > tempb){
				matrix.get(i-1).set(j, tempa + tempc);
			}else{
				matrix.get(i-1).set(j, tempb + tempc);
			}
		}
	}
	System.out.println("Largest sum: "+matrix.get(0).get(0));
	
	System.exit(0);
	////------------------------------
}
}
