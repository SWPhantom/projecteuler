var isDecimalPalindrome = function(decimal){
    var tempDecimal = decimal;
    var reverse = 0;
    while (tempDecimal > 0) {
         reverse = reverse * 10 + Math.floor(tempDecimal % 10);
         tempDecimal = Math.floor(tempDecimal / 10);
    }
    if(decimal != reverse){
        return false;
    }
    
    return true;
}

var isBinaryPalindrome = function(decimal){
    var binaryString = decimal.toString(2);
    for(var i = 0; i < binaryString.length / 2; ++i){
        var j = binaryString.length - 1 - i;
        if(binaryString[i] != binaryString[j]){
            return false;
        }
    }
    
    return true;
}

var main = function(upperLimit) {
    var start = +new Date();
    var sum = 0;
    for(var i = 1; i < upperLimit; ++i){
        if(isDecimalPalindrome(i) && isBinaryPalindrome(i)){
            sum += i;
        }
    }
    
    console.log(sum);
    
    var end =  +new Date();
    console.log(end - start);
}

main(1000000);