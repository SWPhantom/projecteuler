var main = function() {
    var largeString = "";
    for(var i = 1; i < 200000; ++i){
        largeString = largeString.concat(i);
    }
    
    var output = 1;
    for(var i = 1; i <= 1000000; i *= 10){
        output *= parseInt(largeString[i - 1])
    }
    console.log(output);
}

main();