import java.util.*;
import java.math.*;

/**
 * This class is a helper class for checking to see how
 * many divisors an input number has.
 */

public class OneChildHelper{
	private BigInteger number;
	private Long length;
	private String lengthStr;
	private HashMap<String, Integer> subs;
	
	/**
	 * Empty Constructor. Initialize heap instances.
	 */
	public OneChildHelper(){
		subs = new HashMap();
	}
	
	/**	
	 * @param input	-The integer passed from main routine.
	 * @return 			-Returns boolean corresponding to the
	 *						 input having a "one-child divisor".
	 */
	public boolean hasOneChild(BigInteger input){
		if(!subs.isEmpty()){
			subs.clear();
		}
		number = input;
		createSubs();
		int divides = 0;	//This will keep track of how many numbers can  be divided
								//bt the length of the input;
		BigInteger nothing = new BigInteger("0");
		for(String i : subs.keySet()){
		//System.out.println("IN INSTANCE: Number "+i+" is being analyzed.");
			BigInteger temp = new BigInteger(i);
			if(temp.mod(new BigInteger(lengthStr)).compareTo(nothing) == 0){
				divides += subs.get(i);
			}
			if(divides > 1){
				return false;
			}
		}
		if(divides == 1){
			return true;
		}
		return false;
	}
	
	private void createSubs(){
		//Long to string!
		String tempStr = number.toString();
		length = new Long((long)tempStr.length());
		lengthStr = length.toString();
		
		//Doing brute-force n^2 loop of making all substrings.
		for(int i = 0; i < tempStr.length(); ++i){
			for(int j = i+1; j <= tempStr.length(); ++j){
				String tempSubstring = tempStr.substring(i,j);
				if(subs.containsKey(tempSubstring)){
					subs.put(tempSubstring, subs.get(tempSubstring)+1);
				}else{
					subs.put(tempSubstring, 1);
				}
			}
		}
		
		
	}

}
