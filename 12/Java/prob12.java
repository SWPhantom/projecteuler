import java.util.*;
import java.math.*;

public class prob12{
public static void main(String[] args){
	////Description:
	System.out.println("\nThis program takes a command line input 'n'.");
	System.out.println("It then finds the first triangle number that");
	System.out.println("has 'n' divisors.");
	System.out.println("Inspect source code for more information.");
	////------------------------------

	////Declarations:
	int input = 0;
	int output = 0;
	DivHelper divCheck = new DivHelper();
	
	int triangleNum = 0;
	////------------------------------

	////sanity check single argument input block.
	if (args.length == 1){
			try{
				input = Integer.parseInt(args[0]);
			}catch (NumberFormatException e) {
				System.err.println("Argument must be an integer. Exiting.");
				System.exit(1);
			}
	}else{
		System.out.println("Needs 1 argument!");
		System.exit(1);
	}
	////------------------------------
	
	////Hard work block
	for(int i = 1; i < Integer.MAX_VALUE; ++i){//Huge loop! OH NO.
		triangleNum += i;
		int temp = divCheck.checkNumDivisors(triangleNum);
		if(i%50==0){
			System.out.println("\nCHECKING: "+triangleNum);//Output so I'm not feeling like all is broken.
			System.out.println("Divisors: "+temp);
		}
			if(temp >= input){
			System.out.println("\nTriangle number with "+input+" divisors: "+triangleNum+".");
			System.exit(0);
		}
	}
	////------------------------------
}
}
