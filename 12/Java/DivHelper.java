import java.util.*;
/**
 * This class is a helper class for checking to see how
 * many divisors an input number has.
 * @param input	-The integer passed from main routine.
 * @return 			-Returns the number of divisors the
 *						 input has as an integer.
 */

public class DivHelper{
	public DivHelper(){}
	public int checkNumDivisors(int input){
		int divisors = 0;
		for(int i = 1; i <= ((int)Math.sqrt(input)+1);++i){
			if(input%i == 0){
				//System.out.println(i+" is a divisor!");
				divisors += 2;
			}
		}
		return divisors;
	}

}
