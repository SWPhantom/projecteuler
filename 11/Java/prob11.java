import java.util.*;
import java.io.*;
import java.math.*;

public class prob11{
public static void main(String[] args){
	////Description:
	/*
		http://projecteuler.net/problem=11
	*/
	System.out.println(	"\n----------------------------------------------"+
								"\nThis program reads an input text specified via"+
								"\ncommand line. It reads the numbers within and"+
								"\nconstructs an ArrayList for every row of"+
								"\nnumbers. It then calculates the largest product"+
								"\nof 4 numbers in a row, column, or diagonal."+
								"\nInspect source code for more information."+
								"\n----------------------------------------------\n");
	////-------------------------------

	////Declarations:
	String input = "";
	ArrayList<ArrayList<Integer>> matrix = new ArrayList<ArrayList<Integer>>();
	ArrayList<String> rawStrings = new ArrayList<String>();
	long maxProduct = 0;
	////------------------------------

	////sanity check single argument input block.
	if (args.length == 1){
		input = args[0];
	}else{
		System.out.println("Needs 1 argument!");
		System.exit(1);
	}
	////------------------------------
	
	////Hard work block
	
	try {//Block to take in the contents of the file
		BufferedReader in = new BufferedReader(new FileReader(input));
		String str;
		while ((str = in.readLine()) != null)
			rawStrings.add(str);
			in.close();
	}catch(IOException e){}
	
	//Put the input strings into the int array.
	for(int i = 0; i < rawStrings.size(); ++i){
		String [] fields = rawStrings.get(i).split(" ");
		ArrayList<Integer> row = new ArrayList<Integer>();
		row.clear();
		for(int j = 0; j < fields.length; ++j){
			row.add(Integer.parseInt(fields[j]));
		}
		matrix.add(row);
	}
	/*
	//Debug
	for(int i = 0; i < matrix.size(); ++i){
		for(int j = 0; j < matrix.get(i).size(); ++j){
			System.out.print(matrix.get(i).get(j)+".");
		}
		System.out.println();
	}
	*/
	
	//Horizontal checks
	for(int i = 0; i < matrix.size(); ++i){
		for(int j = 0; j < matrix.size()-3; ++j){
			//System.out.println("Checking <"+i+","+j+">");
			long tempProd = matrix.get(i).get(j)*matrix.get(i).get(j+1)*matrix.get(i).get(j+2)*matrix.get(i).get(j+3);
			if(tempProd > maxProduct){
				maxProduct = tempProd;
			}
		}
	}
	//Vertical checks
	for(int i = 0; i < matrix.size()-3; ++i){
		for(int j = 0; j < matrix.size(); ++j){
			//System.out.println("Checking <"+i+","+j+">");
			long tempProd = matrix.get(i).get(j)*matrix.get(i+1).get(j)*matrix.get(i+2).get(j)*matrix.get(i+3).get(j);
			if(tempProd > maxProduct){
				maxProduct = tempProd;
			}
		}
	}
	//Diagonal down checks
	for(int i = 0; i < matrix.size()-3; ++i){
		for(int j = 0; j < matrix.size()-3; ++j){
			//System.out.println("Checking <"+i+","+j+">");
			long tempProd = matrix.get(i).get(j)*matrix.get(i+1).get(j+1)*matrix.get(i+2).get(j+2)*matrix.get(i+3).get(j+3);
			if(tempProd > maxProduct){
				maxProduct = tempProd;
			}
		}
	}
	//Diagonal up checks
	for(int i = 0; i < matrix.size()-3; ++i){
		for(int j = 3; j < matrix.size(); ++j){
			//System.out.println("Checking <"+i+","+j+">");
			long tempProd = matrix.get(i).get(j)*matrix.get(i+1).get(j-1)*matrix.get(i+2).get(j-2)*matrix.get(i+3).get(j-3);
			if(tempProd > maxProduct){
				maxProduct = tempProd;
			}
		}
	}
	
	System.out.println("Largest sum: "+maxProduct);
	
	System.exit(0);
	////------------------------------
}
}
